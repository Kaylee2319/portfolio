import "../css/NavBar.css";
import HamBurgerMenu from "./HamBurgerMenu";
import k from "../assets/k.png";
import cat2 from "../assets/cat2.png";

function NavBar() {
  return (
    <>
      <div className="navBar">
        <div className="navbar1">
          <div>
            <a href="/">
              <img className="navBarLogo" src={k} alt="Kaylee's Logo" />
            </a>
          </div>
        </div>
        <div className="navBarLinks">
          <div className="aboutLink">
            <a href="/projects">Projects</a>
          </div>
          <div className="aboutLink">
            <a href="/about">About Me</a>
          </div>
          <div className="aboutLink">
            <a href="/resume">Resume</a>
          </div>
          <div className="navContactLinks">
            <a className="navContact" href="/contact">
              Contact Me
            </a>
          </div>
        </div>
        <div className="navBar2">
          <HamBurgerMenu />
        </div>
      </div>
      <a href="cats">
        <img className="navCat" src={cat2} alt="peeking cat" />
      </a>
    </>
  );
}

export default NavBar;
