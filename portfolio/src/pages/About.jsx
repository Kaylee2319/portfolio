import React, { useEffect, useRef, useState } from "react";
import "../css/About.css";
import NavBar from "../components/NavBar";
import Footer from "../components/Footer";
import portFishing from "../assets/portFishing.jpg";

function About() {
  const [fadeIn, setFadeIn] = useState(false);
  useEffect(() => {
    document.addEventListener("DOMContentLoaded", function () {
      const boutMeElement = document.querySelector(".boutMe");
      boutMeElement.classList.add("fadeIn");
    });
    setFadeIn(true);
  }, []);
  return (
    <>
      <NavBar />
      <div className="aboutPage">
        <div className="aboutHeader1">
          <h1 className="aboutHeader">About Me</h1>
        </div>
        <div className="boutBodyContent">
          <img className="fishingPhoto" src={portFishing} />
          <div className={`aboutMePara boutMe ${fadeIn ? "fadeIn" : ""}`}>
            <p>
              I'm Kaylee Harding, a 28-year-old veteran who served five years in
              the US Coast Guard, and I'm embarking on an exciting career
              transition to specialize in Front-End Development. My military
              service has instilled in me a strong sense of discipline,
              teamwork, and problem-solving, qualities that I bring to the
              dynamic world of technology.
            </p>
            <p>
              As I shift gears to become a Front-End Software Engineer, I
              leverage my commitment to excellence and a keen eye for detail to
              craft visually appealing and responsive web applications.
              Proficient in HTML, CSS, and JavaScript, I'm passionate about
              staying on the cutting edge of front-end technologies and design
              trends, making continuous learning a cornerstone of my approach.
            </p>
            <p>
              Collaboration is at the heart of my work ethos, allowing me to
              seamlessly work with cross-functional teams and communicate
              effectively. My goal is to deliver impactful and user-centric
              solutions that enhance the overall user experience.
            </p>
            <p>
              My journey from the Coast Guard to the forefront of technology
              reflects my adaptability and determination. I'm driven by a
              passion for creating seamless, engaging user interfaces, and I am
              poised to make significant contributions as a Front-End Software
              Engineer. My unique blend of military precision and creative flair
              positions me as an ideal candidate for organizations seeking a
              dedicated professional in front-end development.
            </p>
          </div>
        </div>
      </div>
      <Footer />
    </>
  );
}

export default About;
