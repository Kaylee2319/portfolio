import React, { useEffect } from "react";
import ReactAudioPlayer from "react-audio-player";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import "../css/MainPage.css";
import NavBar from "../components/NavBar";
import PortSub from "../assets/PortSub.mp3";
import Footer from "../components/Footer";
import KayleesResume from "../assets/KayleesResume.png";
import headshot1 from "../assets/headshot1.png";
import "../css/Projects.css";

function MainPage() {
  useEffect(() => {
    const titles = document.querySelectorAll(
      ".mainPageTitle1, .mainPageTitle2, .mainPageTitle3, .mainPageTitleSlogan, .mainPageTitleSloganTitle, .cvButtonText"
    );

    const delay = setTimeout(() => {
      titles.forEach((title) => title.classList.add("show"));
    }, 500);

    return () => clearTimeout(delay);
  }, []);

  const download = (e) => {
    console.log(e.target.href);
    fetch(e.target.href, {
      method: "GET",
      headers: {},
    })
      .then((response) => {
        response.arrayBuffer().then(function (buffer) {
          const url = window.URL.createObjectURL(new Blob([buffer]));
          const link = document.createElement("a");
          link.href = url;
          link.setAttribute("download");
          document.body.appendChild(link);
          link.click();
        });
      })
      .catch((err) => {
        console.log(err);
      });
  };
  return (
    <>
      <NavBar />
      <ReactAudioPlayer
        autoPlay
        controlsList="nodownload nodirection noplayback"
        src={PortSub}
        className="custom-audio"
        controls
      />
      <div className="mainPageTitleMain">
        <div className="mainPageTitleMain1">
          <div className="mainPageTitle">
            <h1 className="mainPageTitle1">Hi, I'm</h1>
            <h1 className="mainPageTitle2">Kaylee</h1>
            <h1 className="mainPageTitle3">Harding</h1>
          </div>
          <h3 className="mainPageTitleSloganTitle">
            &lt;h3&gt; Software Engineer &lt;/h3&gt;
          </h3>
          <p className="mainPageTitleSlogan">
            &lt;p&gt; With the love of React, HTML, CSS. <br /> I can provide
            pixel perfect design. &lt;p&gt;
          </p>
          <a href={KayleesResume} download onClick={(e) => download(e)}>
            <i className="fa fa-download" />
            <button className="cvButtonText">Download cv</button>
          </a>
        </div>
        <div>
          <img className="headShot" src={headshot1} alt="Head Shot" />
        </div>
      </div>

      <div className="mainPageTech">
        <div className="scrolling-banner">
          <ul className="list1">
            <li className="colorSkills">React</li>
            <li>Python</li>
            <li className="colorSkills">JavaScript</li>
            <li>Git</li>
            <li>Docker</li>
            <li className="colorSkills">CSS</li>
            <li>Figma</li>
            <li>Rest</li>
            <li>Django</li>
            <li className="colorSkills">HTML</li>
            <li>Insomnia</li>
            <li className="mongoDB">MongoDB</li>
          </ul>
          <ul className="list1">
            <li className="colorSkills">React</li>
            <li>Python</li>
            <li className="colorSkills">JavaScript</li>
            <li>Git</li>
            <li>Docker</li>
            <li className="colorSkills">CSS</li>
            <li>Figma</li>
            <li>Rest</li>
            <li className="colorSkills">Django</li>
            <li>HTML</li>
            <li>Insomnia</li>
            <li className="mongoDB1">MongoDB</li>
          </ul>
        </div>
      </div>
      <Footer />
    </>
  );
}

export default MainPage;
