import React from "react";
import "../css/Footer.css";
import gitLogo from "../assets/gitLogo.png";
import linkedin from "../assets/linkedin.png";
import GitLab from "../assets/GitLab.png";
import CV from "../assets/CV.png";

function Footer() {
  return (
    <>
      <div className="footer">
        <div className="footer1">
          <div className="footerWordLinks">
            <a href="/">Home</a>
            <a href="/resume">Resume</a>
            <a href="/about">About Me</a>
            <a href="/contact">Contact Me</a>
          </div>
          <div className="footerPicLinks">
            <a href="/resume">
              <img className="footerImg" src={CV} alt="CV Logo" />
            </a>
            <a href="https://gitlab.com/Kaylee2319">
              <img className="footerImg" src={GitLab} alt="GitLab Logo" />
            </a>
            <a href="https://github.com/Kaylee2319">
              <img className="footerImg" src={gitLogo} alt="GitHub Logo" />
            </a>
            <a href="https://www.linkedin.com/in/kayleeharding/">
              <img className="footerImg" src={linkedin} alt="Linked in Logo" />
            </a>
          </div>
        </div>
        <div className="lineInFooter"></div>
        <div className="footerContactInfo">
          <a href="tel:+14094334439">
            <p>(409) 433-4439</p>
          </a>
          <a href="mailto:kayleeharding@gmail.com">
            <p>KayleeHarding@gmail.com</p>
          </a>
        </div>
      </div>
    </>
  );
}

export default Footer;
