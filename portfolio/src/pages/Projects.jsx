import "../css/Projects.css";
import NavBar from "../components/NavBar";
import Footer from "../components/Footer";
import React, { useEffect, useRef, useState } from "react";
import { useInView } from "react-intersection-observer";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import "../css/MainPage.css";
import Prestige from "../assets/Prestige.mp4";
import prestigeFig from "../assets/prestigeFig.png";
import prestigeChart from "../assets/prestigeChart.png";
import "../css/Projects.css";
import headshotNOBG from "../assets/headshotNOBG.png";

const ImageModal = ({ isOpen, onClose, imageUrl, imageAlt }) => {
  if (!isOpen) return null;

  return (
    <div className="modal close" onClick={onClose}>
      <div className="modal-content">
        <img src={imageUrl} alt={imageAlt} />
      </div>
    </div>
  );
};

const PrevArrow = (props) => {
  const { className, onClick } = props;
  return <div className={`${className} custom-prev-arrow`} onClick={onClick} />;
};

const NextArrow = (props) => {
  const { className, onClick } = props;
  return <div className={`${className} custom-next-arrow`} onClick={onClick} />;
};

function Projects() {
  const descriptionRef1 = useRef(null);
  const [ref, inView] = useInView({
    triggerOnce: true,
    rootMargin: "-100px 0px",
  });
  const [modalOpen, setModalOpen] = useState(false);
  const [selectedItem, setSelectedItem] = useState(null);

  const openModal = (item) => {
    setSelectedItem(item);
    setModalOpen(true);
  };

  const closeModal = () => {
    setModalOpen(false);
  };
  const settings1 = {
    dots: false,
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 1000,
    cssEase: "linear",
    pauseOnHover: false,
    pauseOnFocus: false,
    pauseOnDotsHover: false,
    arrows: false,
  };

  const carouselItems = [
    {
      title: "Prestige Motor Plaza",
      videoSrc: Prestige,
      description:
        "Powered by React for a responsive front end and Django for a robust backend, This platform redefines digital engagement for automotive businesses. It's not just an app; it's a sophisticated engineering marvel.",
      gitLabLink: "https://gitlab.com/Kaylee2319/project-beta",
    },
    {
      title: "Prestige Motor Plaza",
      videoSrc: Prestige,
      description:
        "Powered by React for a responsive front end and Django for a robust backend, This platform redefines digital engagement for automotive businesses. It's not just an app; it's a sophisticated engineering marvel.",
      gitLabLink: "https://gitlab.com/Kaylee2319/project-beta",
    },
  ];

  const settings = {
    arrow: true,
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    prevArrow: <PrevArrow />,
    nextArrow: <NextArrow />,
  };
  const carouselItems2 = [
    {
      title: "Prestige Figma",
      type: "image",
      content: prestigeFig,
    },
    {
      title: "Prestige Bounded Context",
      type: "image",
      content: prestigeChart,
    },
  ];

  const settings3 = {
    arrows: false,
    dots: true,
    infinite: true,
    speed: 1500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 5000,
  };

  useEffect(() => {
    if (inView) {
      const description1 = descriptionRef1.current;
      const lines1 = description1.innerHTML.split(".");
      description1.innerHTML = lines1
        .map(
          (line, index) =>
            `<span class="line" style="animation-delay: ${
              index * 0.2
            }s">${line}</span>`
        )
        .join("<br>");
    }
  }, [inView]);

  return (
    <>
      <NavBar />
      <div className="projectPage">
        <div className="projectHeader1">
          <h1 className="projectHeader">Projects</h1>
        </div>
        <div>
          <Slider {...settings}>
            {carouselItems.map((item, index) => (
              <div key={index}>
                <h3 className="prestigeTitle">{item.title}</h3>
                <div className="prestigeVideo1">
                  <video className="prestigeVideo" controls>
                    <source src={item.videoSrc} />
                    Your browser does not support the video tag.
                  </video>
                </div>
                <div className="prestigeMotors">
                  <div className="prestigeDiscription">
                    <div>
                      <p>{item.description}</p>
                    </div>
                    <div className="gitLabButton">
                      <a className="gitLabButton1" href={item.gitLabLink}>
                        GitLab Repository
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            ))}
          </Slider>
        </div>

        <div className="techStuff">
          <div className="techImg">
            <p
              className="techDiscription"
              ref={(node) => {
                descriptionRef1.current = node;
                ref(node);
              }}
            >
              <h3>Technologies</h3>
              As a Full Stack Developer, my expertise lies in React and
              front-end development, complemented by a strong command of
              JavaScript. I specialize in crafting intuitive and visually
              engaging user interfaces to ensure a seamless and immersive
              experience. On the back end, my preferred technologies are Python
              and Django, allowing me to create robust and scalable solutions.
              With a keen eye for detail and a commitment to delivering
              high-quality code, I thrive on bridging the gap between
              user-centric design and efficient functionality. My versatile
              skill set positions me to contribute effectively to diverse
              projects, ensuring the successful implementation of end-to-end
              solutions in the ever-evolving landscape of web development.
            </p>
            <div className="projectImg">
              <div className="portCarousel">
                <Slider {...settings3}>
                  {carouselItems2.map((item, index) => (
                    <div key={index} onClick={() => openModal(item)}>
                      {item.type === "video" ? (
                        <div className="prestigeImages">
                          <video className="prestigeImage1" controls>
                            <source src={item.content} />
                            Your browser does not support the video tag.
                          </video>
                        </div>
                      ) : (
                        <div className="prestigeImage">
                          <img
                            className="prestigeImage1"
                            src={item.content}
                            alt={item.title}
                          />
                        </div>
                      )}
                    </div>
                  ))}
                </Slider>
                <ImageModal
                  isOpen={modalOpen}
                  onClose={closeModal}
                  imageUrl={selectedItem?.content}
                  imageAlt={selectedItem?.title}
                />
              </div>
              <img className="projectHeadShot" src={headshotNOBG} />
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </>
  );
}

export default Projects;
