import "../css/Contact.css";
import NavBar from "../components/NavBar";
import Footer from "../components/Footer";
import React, { useRef, useState } from "react";
import emailjs from "@emailjs/browser";
import gitLogo from "../assets/gitLogo.png";
import linkedin from "../assets/linkedin.png";
import GitLab from "../assets/GitLab.png";
import CV from "../assets/CV.png";

function Contact() {
  const form = useRef();
  const [isSent, setIsSent] = useState(false);

  const sendEmail = (e) => {
    e.preventDefault();

    emailjs
      .sendForm(
        "service_0nd4gn5",
        "template_9nb1bgi",
        form.current,
        "D_ESkV8X8fOdeLUqc"
      )
      .then(
        (result) => {
          console.log(result.text);
          console.log("message sent");
          setIsSent(true);
        },
        (error) => {
          console.log(error.text);
        }
      );
  };

  return (
    <>
      <NavBar />
      <div className="contactPage">
        <div className="contactHearder">
          <p>Contact Me</p>
        </div>
        <div className="contactPage2">
          <div className="contactPage3">
            <div className="contactInfo">
              <p className="contactInfoTitle">Get in Touch</p>
              <p className="contactInfopPara">
                "Hey! Thanks for checking out my portfolio. I'd love to hear
                from you—whether it's about a project, collaboration, or just to
                say hi. Drop me a line through the form or hit me up on social
                media. Looking forward to connecting with you!"
              </p>
            </div>
            <div className="contactEmail">
              <p className="contactEmailPhone">Email:</p>
              <a href="mailto:kayleeharding@gmail.com">
                <p>Kayleeharding@gmail.com</p>
              </a>
            </div>
            <div className="contactPhone">
              <p className="contactEmailPhone">Phone:</p>
              <a href="tel:+14094334439">
                <p>(409)433-4439</p>
              </a>
            </div>
            <div className="contactPicLinks">
              <a href="/resume">
                <img className="contactImg" src={CV} alt="CV Logo" />
              </a>
              <a href="https://gitlab.com/Kaylee2319">
                <img className="contactImg" src={GitLab} alt="GitLab Logo" />
              </a>
              <a href="https://github.com/Kaylee2319">
                <img className="contactImg" src={gitLogo} alt="GitHub Logo" />
              </a>
              <a href="https://www.linkedin.com/in/kayleeharding/">
                <img
                  className="contactImg"
                  src={linkedin}
                  alt="Linked in Logo"
                />
              </a>
            </div>
          </div>
          <div>
            <div className="contactForm">
              {isSent ? (
                <p className="confirmationMessage">
                  Thank you for your message. I appreciate your interest in my
                  work. I look forward to speaking with you!
                </p>
              ) : (
                <form ref={form} onSubmit={sendEmail}>
                  <label>Name</label>
                  <input type="text" name="user_name" />
                  <label>Email</label>
                  <input type="email" name="user_email" />
                  <label>Message</label>
                  <textarea name="message" />
                  <input className="contactButton" type="submit" value="Send" />
                </form>
              )}
            </div>
          </div>
        </div>
        <Footer />
      </div>
    </>
  );
}

export default Contact;
