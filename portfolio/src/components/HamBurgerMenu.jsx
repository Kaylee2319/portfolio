import React, { useState } from "react";
import "../css/HamBurgerMenu.css";

const HamBurgerMenu = () => {
  const [isOpen, setIsOpen] = useState(false);
  const toggleMenu = () => {
    setIsOpen(!isOpen);
  };

  return (
    <div className="hamburger-menu">
      <div className={`menu-icon ${isOpen ? "open" : ""}`} onClick={toggleMenu}>
        <div className="bar"></div>
        <div className="bar"></div>
        <div className="bar"></div>
      </div>

      {isOpen && (
        <div className="menu-items">
          <div className="menu-1">
            <a href="/about">About</a>
          </div>
          <div className="menu-1">
            <a href="/contact">Contact</a>
          </div>
          <div className="menu-1">
            <a href="/resume">Resume</a>
          </div>
          <div className="menu-1 menuMargin">
            <a href="/projects">Projects</a>
          </div>
        </div>
      )}
    </div>
  );
};

export default HamBurgerMenu;
