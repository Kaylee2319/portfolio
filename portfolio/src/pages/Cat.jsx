import React, { useState, useEffect } from "react";
import "../css/Cat.css";
import NavBar from "../components/NavBar";
import Footer from "../components/Footer";

function Cat() {
  const [catImages, setCatImages] = useState([]);
  const [catImages1, setCatImages1] = useState([]);
  useEffect(() => {
    const fetchCatImages = async () => {
      try {
        const response = await fetch(
          "https://api.thecatapi.com/v1/images/search?limit=400"
        );
        const data = await response.json();
        setCatImages(data);
      } catch (error) {
        console.error("Error fetching cat images:", error);
      }
    };
    const fetchCatImages2 = async () => {
      try {
        const response = await fetch(
          "https://api.thecatapi.com/v1/images/search?limit=400"
        );
        const data = await response.json();
        setCatImages1(data);
      } catch (error) {
        console.error("Error fetching cat images:", error);
      }
    };
    fetchCatImages2();
    fetchCatImages();
  }, []);

  return (
    <>
      <NavBar />
      <div className="mainCat">
        <div className="catHeader1">
          <h1 className="catHeader">Cats</h1>
        </div>
        <div className="catPage">
          <div className="cat-container">
            {catImages.map((cat, index) => (
              <img
                key={index}
                src={cat.url}
                alt={`Cat ${index + 1}`}
                className="cat-image"
              />
            ))}
          </div>
          <div className="cat-container">
            {catImages1.map((cat, index) => (
              <img
                key={index}
                src={cat.url}
                alt={`Cat ${index + 1}`}
                className="cat-image"
              />
            ))}
          </div>
        </div>
        <Footer />
      </div>
    </>
  );
}

export default Cat;
