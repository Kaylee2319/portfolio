import "../css/Resume.css";
import NavBar from "../components/NavBar";
import Footer from "../components/Footer";
import KayleesResume from "../assets/KayleesResume.png";
import { SlCloudDownload } from "react-icons/sl";

function ResumePage() {
  const download = (e) => {
    console.log(e.target.href);
    fetch(e.target.href, {
      method: "GET",
      headers: {},
    })
      .then((response) => {
        response.arrayBuffer().then(function (buffer) {
          const url = window.URL.createObjectURL(new Blob([buffer]));
          const link = document.createElement("a");
          link.href = url;
          link.setAttribute("download");
          document.body.appendChild(link);
          link.click();
        });
      })
      .catch((err) => {
        console.log(err);
      });
  };
  return (
    <>
      <NavBar />
      <div className="resumePage">
        <div className="resumeHeader1">
          <h1 className="resumeHeader">Resume</h1>
        </div>

        <div className="resumeIcon">
          <a href={KayleesResume} download onClick={(e) => download(e)}>
            <i className="fa fa-download" />
            <SlCloudDownload />
          </a>
          <div>
            <img
              className="resumePic"
              src={KayleesResume}
              alt="Kaylees Resume"
            />
          </div>
        </div>
        <Footer />
      </div>
    </>
  );
}

export default ResumePage;
